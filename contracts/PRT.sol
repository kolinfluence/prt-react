// contracts/access-control/Auth.sol
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;


import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@openzeppelin/contracts/finance/PaymentSplitter.sol";
import "@openzeppelin/contracts/utils/cryptography/MerkleProof.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/utils/Strings.sol";

contract PRT is Ownable, ReentrancyGuard {

    using Strings for uint256;
    

    using Counters for Counters.Counter;
    Counters.Counter public _tokenPRTID_index;

    uint256 public constant PRTID = 20000;
    uint256 public constant MAX_BUYABLE_AMOUNT = 100;
    uint256 public constant MAX_PRT_INDEX = 180000;
    bool public presalePRT = false;
    
// Public Raffles Ticket (PRT) : 160,000
// ID #20,001 to ID #180,000
    mapping(address => uint256[]) userPRTs; // we can not keep in memory max 160000 counts . needs find solution

    constructor() {
       
    }
    
    event DitributePRTs(uint256 indexed PRT_ID, address indexed from, uint spent, uint total_amount_per_transaction); /* This is an event */
    // event AmountBuyableLeft(address indexed from, uint256 amount_buyable_left);

    modifier onlyAccounts () {
        require(msg.sender == tx.origin, "Not allowed origin");
        _;
    }


    function toggleSale() public onlyOwner {
        presalePRT = !presalePRT;
    }

     function buyPRT (address account, uint amount_wanted) external payable onlyAccounts {
        require(account != owner(), "Owner of contract can not buy PRT");
        require(msg.sender == account,"Not allowed");

        require(presalePRT, "Sale PRT is OFF");



        require(
            amount_wanted <= MAX_BUYABLE_AMOUNT,      "You can't mint so much tokens");


        uint256 _presaleClaimedAmount = userPRTs[account].length;
        require(_presaleClaimedAmount > 0, "Safe check up");
        require(_presaleClaimedAmount < MAX_BUYABLE_AMOUNT, "You already participate in tickets sale and acceded 100 amount");

        require(
            _presaleClaimedAmount + amount_wanted <= MAX_BUYABLE_AMOUNT,  "You can't mint so much tokens");


        //recalculate amount_wanted if needs
        uint256 _amount_wanted_able_to_get = amount_wanted;
        if (_tokenPRTID_index.current() + PRTID + amount_wanted > MAX_PRT_INDEX) {  
            _amount_wanted_able_to_get = MAX_PRT_INDEX - _tokenPRTID_index.current() - PRTID; 
        }

        require(_amount_wanted_able_to_get > 0, "Amount buyable left needs to be greater than 0");
         
        require(_presaleClaimedAmount + _amount_wanted_able_to_get <= MAX_BUYABLE_AMOUNT, "You amount_wanted is greater then expected");


         uint weiBalanceWallet = msg.value;
         require(weiBalanceWallet > 0.01 ether, "Minumum 0.01 ether");
         require(0.01 ether * _amount_wanted_able_to_get <= weiBalanceWallet, "You do not have such amount on yoru balance");


        uint256 the_last_index_wanted = _tokenPRTID_index.current() + _amount_wanted_able_to_get + PRTID;
        require(the_last_index_wanted >= 20001 && the_last_index_wanted <= MAX_PRT_INDEX, "The Last ID PRT token is not correct");    

        //pay first to owner of contract 
        transferInternal(_amount_wanted_able_to_get);

        //update userPRTs[account]
        for (uint i = 0; i < _amount_wanted_able_to_get; i++) {
           distributePRTInternal(i, _amount_wanted_able_to_get);
        }

     }

    function distributePRTInternal(uint _i, uint _amount) internal nonReentrant {//That means the function has been called only once (within the transaction). 
        _tokenPRTID_index.increment();
        uint256 tokenPRTID = _tokenPRTID_index.current() + PRTID;
        
        //release PRT by update status
        userPRTs[msg.sender].push(tokenPRTID);

        emit DitributePRTs(tokenPRTID, msg.sender, 0.01 ether * _i, _amount);
        // _safeMint(..);
    }


    function transferInternal(uint _amount) internal nonReentrant {//That means the function has been called only once (within the transaction). 
        payable(owner()).transfer(0.01 ether * _amount);// bulk send money from sender to owner

    }


     function withdraw () public payable onlyOwner {
         payable(msg.sender).transfer(address(this).balance);//This function allows the owner to withdraw from the contract

     }
     

}
